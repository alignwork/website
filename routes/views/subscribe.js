'use strict';

const MC = require('../../services/mailchimp');
const Mailer = require('../../services/mailer');

exports = module.exports = async function (req, res) {

	let result, intCat;
	try {
		if (!req.body.email) {
			return res.status(200).json({
				response: {
					text: 'Email address cannot be blank.',
				},
			});
		}
		if (req.body.firstName) {
			let regex = new RegExp(/^[a-zA-Z]/g);
			if (!regex.test(req.body.firstName)) {
				return res.status(200).json({
					response: {
						text: 'First name should only contain letters.',
					},
				});
			}
		}
		if (req.body.interestCategory) {
			switch (req.body.interestCategory) {
				case 'Insights':
					intCat = process.env.MAILCHIMP_AWML_INSIGHTS_ID;
					break;
				case 'Coaching':
					intCat = process.env.MAILCHIMP_AWML_COACHING_ID;
					break;
				case 'eBook':
					intCat = process.env.MAILCHIMP_AWML_EBOOK_ID;
					break;
				case 'Trial':
					intCat = process.env.MAILCHIMP_AWML_TRIAL_ID;
					break;
				default:
					intCat = process.env.MAILCHIMP_AWML_INSIGHTS_ID;
			}
		}

		let memberData = await MC.getMemberDetails();
		let emailIdx = memberData.emails.findIndex(email => email === req.body.email);
		if (emailIdx > -1) {
			// email already registered
			// TODO: currently assumed double opt-in, need to double check and send the opt-in if not
			let interests = memberData.interests[emailIdx];
			let id = memberData.ids[emailIdx];
			let keys = Object.keys(interests);
			keys.forEach(async key => {
				if (intCat === key) {
					if (!interests[key]) {
						// interest category is false, update existing record
						if (intCat === process.env.MAILCHIMP_AWML_TRIAL_ID) {
							result = await MC.editMemberInterests(id, intCat, req.body.userName, req.body.teamName);

							// automatically subscribe user for Insights
							await MC.editMemberInterests(id, process.env.MAILCHIMP_AWML_INSIGHTS_ID);

							// send notification email
							await Mailer.sendTrialNotification(req.body.email, req.body.userName, req.body.teamName);
						} else {
							result = await MC.editMemberInterests(id, intCat);
							await Mailer.sendSubscribeEmail(req.body.interestCategory, req.body.email, req.body.firstName);
						}
						return res.status(200).json(result);
					} else {
						// interest category already set
						return res.status(200).json({
							response: {
								text: 'Email already registered for this type of enquiry, please try another address.',
							},
						});
					}
				}
			});
		} else {
			// email not registered
			result = await MC.addMember(req.body.email, intCat, req.body.firstName, req.body.lastName, req.body.fullName, req.body.userName, req.body.teamName);
			// do not send double opt-in to subscribers for a free trial, as they are automatically set to opted in
			if (intCat !== process.env.MAILCHIMP_AWML_TRIAL_ID) {
				await Mailer.sendDoubleOptIn(intCat, req.body.email, req.body.firstName);
			}
			if (intCat === process.env.MAILCHIMP_AWML_TRIAL_ID) {
				await Mailer.sendTrialNotification(req.body.email, req.body.userName, req.body.teamName);
			}
			return res.status(200).json(result);
		}
	}
	catch (e) {
		console.log(e);
		res.status(200).json(e);
	}
};
