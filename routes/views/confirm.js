'use strict';

const MC = require('../../services/mailchimp');
const Mailer = require('../../services/mailer');

exports = module.exports = async function (req, res) {
	let email = req.query.email;
	let intCat;
	let firstName = req.query.firstname;

	switch (req.query.interest) {
		case process.env.MAILCHIMP_AWML_INSIGHTS_ID:
			intCat = 'Insights';
			break;
		case process.env.MAILCHIMP_AWML_COACHING_ID:
			intCat = 'Coaching';
			break;
		case process.env.MAILCHIMP_AWML_EBOOK_ID:
			intCat = 'eBook';
			break;
	}

	try {
		let memberData = await MC.getMemberDetails();
		let emailIdx = memberData.emails.findIndex(item => item === email);
		if (emailIdx > -1) {
			// email located
			try {
				// change member double-opt-in status to true
				let id = memberData.ids[emailIdx];
				await MC.subscribeMember(id);
				await Mailer.sendSubscribeEmail(intCat, email, firstName);
				res.redirect('/?flash=m2');
			}
			catch (e) {
				throw e;
			}
		} else {
			// email not already registered
			// TODO: need to do something with this error, probably send back the index template with a flash message
			console.log('trying to double-opt in, but email address not found in MC.');
			res.redirect('/');
		}
	}
	catch (e) {
		res.redirect('/');
	}
};
