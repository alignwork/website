'use strict';

const keystone = require('keystone');

exports = module.exports = function (req, res) {

	const view = new keystone.View(req, res);
	const locals = res.locals;

	// locals.section is used to set the currently selected
	// item in the header navigation.
	locals.section = 'coaching';
	locals.data = [];
	locals.title = '';
	locals.snippet = '';

	// Load the faq resources from the db
	view.on('init', next => {
		keystone.list('Coaching')
			.model
			.findOne({ title: 'Align.Work | Coaching' })
			.populate('services')
			.exec((err, result) => {
				locals.data = result;
				locals.title = result.title;
				locals.snippet = result.snippet;
				next(err);
			});
	});

	view.on('init', next => {
		keystone.list('Trial')
			.model
			.findOne({ trial: 'signup-form' })
			.exec((err, result) => {
				locals.data.trial = result;
				next(err);
			});
	});
	// Render the view
	view.render('coaching');
};
