'use strict';

const keystone = require('keystone');
const flashMessages = require('../../flash-messages');

exports = module.exports = function (req, res) {

	const view = new keystone.View(req, res);
	const locals = res.locals;

	if (req.query.flash) {
		// flash message to be displayed
		let flashMessage = req.query.flash;
		if (flashMessages[flashMessage]) {
			req.flash(flashMessages[flashMessage].category, flashMessages[flashMessage].message);
		}
		else {
			req.flash(flashMessages.m0.category, flashMessages.m0.message);
		}

	}

	// locals.section is used to set the currently selected
	// item in the header navigation.
	locals.section = 'home';
	locals.data = [];
	locals.title = '';
	locals.snippet = '';

	// Load the homepage from the db
	view.on('init', next => {
		keystone.list('Homepage')
			.model
			.findOne({ title: 'Align.Work | Home' })
			.populate('slides creds.logos creds.testimonials reference')
			.exec((err, result) => {
				locals.data = result;
				locals.title = result.title;
				locals.snippet = result.snippet;
				next(err);
			});
	});

	view.on('init', next => {
		keystone.list('Ebook')
			.model
			.findOne({ ebook: 'ebook' })
			.exec((err, result) => {
				locals.data.ebook = result;
				next(err);
			});
	});

	view.on('init', next => {
		keystone.list('Trial')
			.model
			.findOne({ trial: 'signup-form' })
			.exec((err, result) => {
				locals.data.trial = result;
				next(err);
			});
	});
	// Render the view
	view.render('index');
};
