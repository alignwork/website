'use strict';

const keystone = require('keystone');

exports = module.exports = function (req, res) {

	let title = req.query.file;
	let link = '';

	keystone.list('DownloadLink')
		.model
		.findOne({ title })
		.exec((err, result) => {
			console.log('result', result);
			if (err || !result) {
				return res.redirect('/?flash=m1');
			}
			link = result.CloudinaryUrl;
			// Redirect to download link on success
			return res.redirect(link);
		});
};
