/* eslint-disable */

var markEmailError = function (interestCategory) {
	switch (interestCategory) {
		case 'Insights':
			$('.footer__subscribeEmailGroup').addClass('has-error');
			break;
		case 'eBook':
			$('.ebook__emailGroup').addClass('has-error');
			break;
		case 'Coaching':
			$('.coaching__emailGroup').addClass('has-error');
			break;
	}
};

var markFirstNameError = function (interestCategory) {
	switch(interestCategory) {
		case 'Insights':
			$('.footer__subscribeFirstNameGroup').addClass('has-error');
			break;
		case 'eBook':
			$('.ebook__firstNameGroup').addClass('has-error');
			break;
		case 'Coaching':
			$('.coaching__firstNameGroup').addClass('has-error');
			break;
	}
};

var displaySuccessMessage = function (interestCategory, submissionType) {
	// TODO: content manage success message
	switch (interestCategory) {
		case 'Insights':
			$('.footer__subscribeForm').remove();
			$('.footer__subscribeMessagesContainer').html('<h3>Thank you for registering for Align.Work insights.</h3>');
			break;
		case 'eBook':
			$('.ebook__form').html('<h4 class="ebook__success">Success! An email containing a download link will be sent to your email address shortly.</h4>');
			break;
		case 'Coaching':
			$('.coaching__form').html('<h4 class="coaching__success">Congratulations, your enquiry was successfully registered. A member of the Align.Work team will be in touch shortly.</h4>');
			break;
		case 'Trial':
			if (submissionType === "modal") {
				$('.trial__form').remove();
				$('.trial__messagesContainer').html('<h4>Success, you subscribed for a free trial. A member of the Align.Work team will be in touch shortly with your login details.</h4><img height="1" width="1" style="display:none;" alt="" src="https://dc.ads.linkedin.com/collect/?pid=157564&conversionId=305284&fmt=gif" /><script>\n' +
					'function gtag_report_conversion(url) {\n' +
					'  var callback = function () {\n' +
					'    if (typeof(url) != \'undefined\') {\n' +
					'      window.location = url;\n' +
					'    }\n' +
					'  };\n' +
					'  gtag(\'event\', \'conversion\', {\n' +
					'      \'send_to\': \'AW-830581813/kc_NCKPXtn8QtdiGjAM\',\n' +
					'      \'event_callback\': callback\n' +
					'  });\n' +
					'  return false;\n' +
					'}\n' +
					'</script>');
			}
			else {
				$('.trial__subscribeForm').remove();
				$('.trial__subscribeMessagesContainer').html('<p>Success, you subscribed for a free trial. A member of the Align.Work team will be in touch shortly with your login details.</p><img height="1" width="1" style="display:none;" alt="" src="https://dc.ads.linkedin.com/collect/?pid=157564&conversionId=305284&fmt=gif" /><script>\n' +
					'function gtag_report_conversion(url) {\n' +
					'  var callback = function () {\n' +
					'    if (typeof(url) != \'undefined\') {\n' +
					'      window.location = url;\n' +
					'    }\n' +
					'  };\n' +
					'  gtag(\'event\', \'conversion\', {\n' +
					'      \'send_to\': \'AW-830581813/kc_NCKPXtn8QtdiGjAM\',\n' +
					'      \'event_callback\': callback\n' +
					'  });\n' +
					'  return false;\n' +
					'}\n' +
					'</script>');
			}
			break;
	}
};

var displayErrorMessage = function (interestCategory, error, submissionType) {
	// TODO: content manage error message
	switch(interestCategory) {
		case 'Insights':
			$('.footer__subscribeButton').attr('disabled', false).text('Try again');
			$('.footer__subscribeMessagesContainer').html('<h5><span class="subscribeError">Tip: </span>' + error + '</h5>');
			break;
		case 'eBook':
			$('.ebook__signupButton').attr('disabled', false).text('Try again');
			$('.ebook__messages').html('<h5><span class="subscribeError">Tip: </span>' + error + '</h5>');
			break;
		case 'Coaching':
			$('.coaching__modalSignupButton').attr('disabled', false).text('Try again');
			$('.coaching__messages').html('<h5><span class="subscribeError">Tip: </span>' + error + '</h5>');
			break;
		case 'Trial':
			if (submissionType === "modal") {
				$('.trial__signupButton').attr('disabled', false).text('Go');
				$('.trial__messages').html('<p><span class="subscribeError">Tip: </span>' + error + '</p>');
			}
			else {
				$('.trial__button').attr('disabled', false).text('Go');
				$('.trial__subscribeMessagesContainer').html('<p><span class="subscribeError">Tip: </span>' + error + '</p>');
			}
			break;
	}
};

var subscribe = function(email, firstName, interestCategory, userName, teamName, submissionType) {
	
	// send subscribe form data to server
	superagent
		.post('/subscribe')
		.send({
			email: email,
			firstName: firstName,
			userName: userName,
			interestCategory: interestCategory,
			teamName: teamName
		})
		.then(function(success) {
			/* 
			Important - the /subscribe endpoint always returns a 200 if it successfully processes
			the request even if there was a validation error, or if there was an error from MailChimp.
			All errors are flagged in the body.status code not the response status code. 
			*/

			console.log(success);
			var body = success.body;
			if (body.status !== 200) throw body;
			displaySuccessMessage(interestCategory, submissionType);
		})
		.catch(function(err) {
			// handle errors from mailchimp and keystone
			var genericError = 'There was a problem subscribing you to Align.Work, please try again later.';
			var error;
			if (err.response && err.response.text) {
				try {
					var mailchimpError = JSON.parse(err.response.text);
					if (mailchimpError.title === 'Member Exists') {
						markEmailError(interestCategory);
						error = 'This email is already registered with Align.Work, please use another address.';
					}
					if (mailchimpError.title === 'Invalid Resource' && mailchimpError.detail === 'Please provide a valid email address.') {
						markEmailError(interestCategory);
						error = "Please provide a valid email address.";
					}
				}
				catch (e) {
					var serverError = err.response.text;
					if (serverError === 'First name should only contain varters.') markFirstNameError(interestCategory);
					if (serverError === 'Email address cannot be blank.') markEmailError(interestCategory);
					error = err.response.text;
				}
			}
			
			if(!error) error = genericError;
			displayErrorMessage(interestCategory, error, submissionType);
		});
};

var formSubmission = function (e, elem, emailClass, firstNameClass, interestCategory, userNameClass, teamNameClass, submissionType) {
	e.preventDefault();
	$(elem).attr('disabled', true).text('Sending');
	var email = $(emailClass).val();
	var firstName = $(firstNameClass).val();
	var username = $(userNameClass).val();
	var teamName = $(teamNameClass).val();
	subscribe(email, firstName, interestCategory, username, teamName, submissionType);
};

$(document).ready(function () {
	
	//LinkedIn tracking
	(function(){
		var s = document.getElementsByTagName("script")[0];
		var b = document.createElement("script");
		b.type = "text/javascript";b.async = true;
		b.src = "https://snap.licdn.com/li.lms-analytics/insight.min.js";
		s.parentNode.insertBefore(b, s);
	})();

	//subscribe functionality
	$('.footer__subscribeButton').on('touchstart click', function (e) {
		formSubmission(e, this, '.footer__subscribeEmail', '.footer__subscribeFirstName', 'Insights');
	});
	
	$('.ebook__signupButton').on('touchstart click', function (e) {
		formSubmission(e, this, '.ebook__email', '.ebook__firstName', 'eBook');
	});
	
	$('.coaching__modalSignupButton').on('touchstart click', function (e) {
		formSubmission(e, this, '.coaching__email', '.coaching__firstName', 'Coaching');
	});
	
	$('.trial__button').on('touchstart click', function (e) {
		formSubmission(e, this, '.trial__email', null, 'Trial', '.trial__username', '.trial__teamname');
	});

	$('.trial__signupButton').on('touchstart click', function (e) {
		formSubmission(e, this, '.trial__modalEmail', null, 'Trial', '.trial__modalUsername', '.trial__modalTeamname', 'modal');
	});

	console.log('jQ loaded, scripts running...');
});

/* eslint-enable */

