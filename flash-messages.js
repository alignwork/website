'use strict';

module.exports = {
	m0: {
		category: 'error',
		message: 'ERROR: No such flash message exists.',
	},
	m1: {
		category: 'error',
		message: 'ERROR: Document with that filename does not exist, cannot download.',
	},
	m2: {
		category: 'warning',
		message: 'Thanks for confirming you are happy to receive email messages from us. Please check your email for a link to the content you requested.',
	},
};
