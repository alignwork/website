const keystone = require('keystone');
const Types = keystone.Field.Types;

/**
 * CoachingService Model
 * =====================
 */

const CoachingService = new keystone.List('CoachingService', {
	map: { name: 'title' },
	autokey: { path: 'service', from: 'title', unique: true },
	track: true,
});

CoachingService.add({
	title: { type: String, required: true, default: 'Service-' },
	heading: { type: String, required: true, default: 'Coaching Service Heading' },
	body: { type: Types.Html, wysiwyg: true, required: true, default: 'Nullam quis risus eget urna mollis ornare vel eu leo. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nullam id dolor id nibh ultricies vehicula. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.' },
	panel: {
		heading: { type: String, required: true, default: 'Syllabus' },
		body: { type: Types.Html, wysiwyg: true, required: true, default: 'Nullam quis risus eget urna mollis ornare vel eu leo. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nullam id dolor id nibh ultricies vehicula. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.' },
	},
	button: {
		body: { type: String, required: true, default: 'Get In Touch' },
	},
});

CoachingService.defaultColumns = 'title, createdAt, createdBy, updatedAt, updatedBy';

CoachingService.register();
