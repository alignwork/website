const keystone = require('keystone');
const Types = keystone.Field.Types;

/**
 * Bio Model
 * =========
 */

const Bio = new keystone.List('Bio', {
	map: { name: 'title' },
	autokey: { path: 'bio', from: 'title', unique: true },
	track: true,
	label: 'Team Bios',
});

Bio.add({
	title: { type: String, required: true, default: '-bio' },
	heading: { type: String, required: true, default: 'Heading' },
	body: { type: Types.Html, wysiwyg: true },
	imageUrl: { type: Types.Url },
	upload: { type: Types.CloudinaryImage },
	link: { type: Types.Url },
	linkLabel: { type: String, required: true, default: 'LinkedIn' },
});

Bio.register();
