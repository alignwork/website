const keystone = require('keystone');
const Types = keystone.Field.Types;

/**
 * Product Model
 * ==============
 */

const Product = new keystone.List('Product', {
	map: { name: 'title' },
	autokey: { path: 'product', from: 'title', unique: true },
	track: true,
	label: 'Product',
});

Product.add('Content', {
	title: { type: String, required: true, default: 'Align.Work | Product' },
	heading: { type: String, required: true, default: 'Product' },
	subHeading: { type: String, required: true, default: 'Some intro copy here' },
	upload: { type: Types.CloudinaryImage },
	imageUrl: { type: Types.Url},
	imageHeading: { type: String, required: false, default: 'Headline for the video'},
	imageParagraph: { type: Types.Html, wysiwyg: true, required: false, default: 'Paragraph text for the video' },
	videoPosterUrl: { type: Types.Url },
	videoMP4Url: { type: Types.Url },
	videoOGGUrl: { type: Types.Url },
	videoWEBMUrl: { type: Types.Url },
	videoHeading: {type: String, required: false, default: 'Headline for the video'},
	videoParagraph: {type: Types.Html, wysiwyg: true, required: false, default: 'Paragraph text for the video' },
	features: { type: Types.Relationship, ref: 'ProductFeature', many: true },
});

Product.add('SEO', {
	snippet: { type: String, required: true, default: 'Insert 160 character meta-description here' },
});

Product.register();
