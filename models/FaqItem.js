const keystone = require('keystone');
const Types = keystone.Field.Types;

/**
 * FaqItem Model
 * =============
 */

const FaqItem = new keystone.List('FaqItem', {
	map: { name: 'title' },
	autokey: { path: 'faqItem', from: 'title', unique: true },
	track: true,
	label: 'FAQ Items',
});

FaqItem.add({
	title: { type: String, required: true, default: 'Faq-Item-' },
	heading: { type: String, required: true, default: 'Heading here' },
	body: { type: Types.Textarea, required: true, default: 'Body here' },
});

FaqItem.register();
