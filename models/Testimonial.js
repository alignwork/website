const keystone = require('keystone');
const Types = keystone.Field.Types;

/**
 * Testimonial Model
 * =================
 */

const Testimonial = new keystone.List('Testimonial', {
	map: { name: 'title' },
	autokey: { path: 'testimonial', from: 'title', unique: true },
	track: true,
	label: 'Testimonials',
});

Testimonial.add({
	title: { type: String, required: true, default: '-testimonial' },
	imageUrl: { type: Types.Url },
	upload: { type: Types.CloudinaryImage },
	quote: { type: String, required: true, default: 'Quotation here...' },
	citation: { type: String, required: true, default: 'Citation here...' },
});

Testimonial.register();
