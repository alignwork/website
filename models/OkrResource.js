const keystone = require('keystone');
const Types = keystone.Field.Types;

/**
 * OkrResource Model
 * =================
 */

const OkrResource = new keystone.List('OkrResource', {
	map: { name: 'title' },
	autokey: { path: 'okr', from: 'title', unique: true },
	track: true,
	label: 'OKR Resources',
});

OkrResource.add({
	title: { type: String, required: true, default: '-resource' },
	heading: { type: String, required: true, default: 'Heading' },
	link: { type: Types.Url },
	body: { type: Types.Html, wysiwyg: true },
	imageUrl: { type: Types.Url },
	upload: { type: Types.CloudinaryImage },
});

OkrResource.register();
