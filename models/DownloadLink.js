const keystone = require('keystone');
const Types = keystone.Field.Types;

/**
 * DownloadLink Model
 * ==================
 */

const DownloadLink = new keystone.List('DownloadLink', {
	map: { name: 'title' },
	autokey: { path: 'link', from: 'title', unique: true },
	track: true,
	label: 'Download Link',
});

DownloadLink.add('Content', {
	title: { type: String, required: true, default: 'Link-' },
	CloudinaryUrl: { type: Types.Url },
});

DownloadLink.register();
