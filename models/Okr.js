const keystone = require('keystone');
const Types = keystone.Field.Types;

/**
 * Okr Model
 * =========
 */

const Okr = new keystone.List('Okr', {
	map: { name: 'title' },
	autokey: { path: 'okr', from: 'title', unique: true },
	track: true,
	label: 'OKR',
});

Okr.add('Content', {
	title: { type: String, required: true, default: 'Align.Work | OKR' },
	heading: { type: String, required: true, default: 'OKR' },
	subHeading: { type: String, required: true, default: 'Learn more about the history and best practice about using the Objectives and Key Results (OKR) methodology' },
});

Okr.add('Benefits', {
	benefits: {
		heading: { type: String, required: true, default: 'Benefits' },
		subHeading: { type: String, required: true, default: 'Correctly implemented, OKRs will improve productivity dramatically.' },
		body: { type: Types.Html, wysiwyg: true, required: true, default: 'Nullam quis risus eget urna mollis ornare vel eu leo.' },
		panel: {
			heading: { type: String, required: true, default: 'Objectives and Key Results (OKR)' },
			objUpload: { type: Types.CloudinaryImage },
			objImageUrl: { type: Types.Url },
			objBody: { type: String, required: true, default: 'Nullam quis risus eget urna mollis ornare vel eu leo. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nullam id dolor id nibh ultricies vehicula. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.' },
			krUpload: { type: Types.CloudinaryImage },
			krImageUrl: { type: Types.Url },
			krBody: { type: String, required: true, default: 'Nullam quis risus eget urna mollis ornare vel eu leo. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nullam id dolor id nibh ultricies vehicula. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.' },
		},
	},
});

Okr.add('How', {
	how: {
		heading: { type: String, required: true, default: 'How are they used?' },
		subHeading: { type: String, required: true, default: 'The following cycle is repeated at regular intervals.' },
		list: { type: Types.Html, wysiwyg: true, default: 'Hello world' },
		upload: { type: Types.CloudinaryImage },
		imageUrl: { type: Types.Url },
	},
});

Okr.add('Implementation', {
	implementation: {
		heading: { type: String, required: true, default: 'Implementation Considerations' },
		subHeading: { type: String, required: true, default: 'Copy for sub heading here.' },
		list: { type: Types.Html, wysiwyg: true, default: 'Hello world' },
		upload: { type: Types.CloudinaryImage },
		imageUrl: { type: Types.Url },
	},
});

Okr.add('Further Reading', {
	furtherReading: {
		heading: { type: String, required: true, default: 'Further Reading' },
		okrResources: { type: Types.Relationship, ref: 'OkrResource', many: true },
	},
});

Okr.add('SEO', {
	snippet: { type: String, required: true, default: 'Insert 160 character meta-description here' },
});


Okr.register();
