const keystone = require('keystone');
const Types = keystone.Field.Types;

/**
 * ProductFeature Model
 * =====================
 */

const ProductFeature = new keystone.List('ProductFeature', {
	map: { name: 'title' },
	autokey: { path: 'feature', from: 'title', unique: true },
	track: true,
});

ProductFeature.add({
	title: { type: String, required: true, default: 'Feature-' },
	heading: { type: String, required: false, default: 'Product Feature Heading' },
	body: { type: Types.Html, wysiwyg: true, required: true, default: 'Nullam quis risus eget urna mollis ornare vel eu leo. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nullam id dolor id nibh ultricies vehicula. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.' },
	upload: { type: Types.CloudinaryImage },
	imageUrl: { type: Types.Url },
});

ProductFeature.defaultColumns = 'title, createdAt, createdBy, updatedAt, updatedBy';

ProductFeature.register();
