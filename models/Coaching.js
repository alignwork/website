const keystone = require('keystone');
const Types = keystone.Field.Types;

/**
 * Coaching Model
 * ==============
 */

const Coaching = new keystone.List('Coaching', {
	map: { name: 'title' },
	autokey: { path: 'coaching', from: 'title', unique: true },
	track: true,
	label: 'Coaching',
});

Coaching.add('Content', {
	title: { type: String, required: true, default: 'Align.Work | Coaching' },
	heading: { type: String, required: true, default: 'Coaching' },
	subHeading: { type: String, required: true, default: 'Some intro copy here' },
	services: { type: Types.Relationship, ref: 'CoachingService', many: true },
	modal: {
		heading: { type: String, required: true, default: 'Coaching Enquiry' },
		body: { type: String, required: true, default: 'We\'ll contact you shortly to arrange a convenient time to connect online.' },
		button: {
			body: { type: String, required: true, default: 'Submit' },
		},
	},
});

Coaching.add('SEO', {
	snippet: { type: String, required: true, default: 'Insert 160 character meta-description here' },
});

Coaching.register();
