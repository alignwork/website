const keystone = require('keystone');

/**
 * Trial Model
 * ===========
 */

const Trial = new keystone.List('Trial', {
	map: { name: 'title' },
	autokey: { path: 'trial', from: 'title', unique: true },
	track: true,
	label: 'Trials',
});

Trial.add({
	title: { type: String, required: true, default: 'Signup-' },
	heading: { type: String, required: true, default: 'Try it free' },
	button: {
		body: { type: String, required: true, default: 'Sign-up' },
	},
	modal: {
		heading: { type: String, required: true, default: 'Sign-up info' },
		button: {
			body: { type: String, required: true, default: 'Start trial' },
		},
	},
});

Trial.register();