const keystone = require('keystone');
const Types = keystone.Field.Types;

/**
 * Footer Model
 * ==============
 */

const Footer = new keystone.List('Footer', {
	map: { name: 'title' },
	autokey: { path: 'footer', from: 'title', unique: true },
	track: true,
	label: 'Footer',
});

Footer.add({
	title: { type: String, required: true, default: 'Footer' },
	subscribe: {
		heading: { type: String, required: true, default: 'Sign-up to Align.Work insights' },
		button: {
			body: { type: String, required: true, default: 'Sign-up' },
		},
	},
	recent: {
		heading: { type: String, required: true, default: 'Recent Posts' },
	},
	links: {
		heading: { type: String, required: true, default: 'Quick Links' },
	},
	contact: {
		heading: { type: String, required: true, default: 'Contacts' },
		// list: { type: Types.Html, wysiwyg: true },
		email: { type: String, required: true, default: 'hello@align.work' },
		phone: { type: String, required: true, default: '+44 20 8144 9964' },
		twitter: { type: String, required: true, default: '@alignwork' },
		twitterLink: { type: Types.Url, required: true, default: 'https://twitter.com/AlignWork' },
		linkedin: { type: String, required: true, default: 'LinkedIn' },
		linkedinLink: { type: Types.Url, required: true, default: 'https://www.linkedin.com/company/18294538/' },
	},
	copyright: {
		body: { type: String, required: true, default: 'Copyright Align.Work 2018' },
	},
});

Footer.register();
