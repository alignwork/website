const keystone = require('keystone');
const Types = keystone.Field.Types;

/**
 * About Model
 * ===========
 */

const About = new keystone.List('About', {
	map: { name: 'title' },
	autokey: { path: 'about', from: 'title', unique: true },
	track: true,
	label: 'About',
});

About.add('Content', {
	title: { type: String, required: true, default: 'About-' },
	heading: { type: String, required: true, default: 'About' },
	sectionHeading1: { type: String, required: true, default: 'Team' },
	sectionHeading2: { type: String, required: true, default: 'Values' },
	bios: { type: Types.Relationship, ref: 'Bio', many: true },
	values: {
		left: {
			heading: { type: String, required: true, default: 'Continually learn and improve' },
			body: { type: String, required: true, default: 'Technology changes at a high pace, continually self improve, create time to learn.  Find new, smarter ways to do things, share them with colleagues in expectation that they will share with you.' },
		},
		top: {
			heading: { type: String, required: true, default: 'Make an impact' },
			body: { type: String, required: true, default: 'Prioritise your work on things that have an impact, be action oriented, strive to get things completed.' },
		},
		right: {
			heading: { type: String, required: true, default: 'Have high standards' },
			body: { type: String, required: true, default: 'Strive to be satisfied with nothing less than the highest standards.  Go the extra mile before being asked, take satisfaction from the quality of your delivery.' },
		},
		bottom: {
			heading: { type: String, required: true, default: 'Take responsibility' },
			body: { type: String, required: true, default: 'Be a self starter, take responsibility, make a decision, learn from mistakes.' },
		},
	},
});

About.add('SEO', {
	snippet: { type: String, required: true, default: 'Insert 160 character meta-description here' },
});

About.register();
