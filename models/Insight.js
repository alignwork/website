const keystone = require('keystone');

/**
 * Insight Model
 * =============
 */

const Insight = new keystone.List('Insight', {
	map: { name: 'title' },
	autokey: { path: 'insight', from: 'title', unique: true },
	track: true,
	label: 'Insight',
});

Insight.add('Content', {
	title: { type: String, required: true, default: 'Align.Work | Insights' },
	heading: { type: String, required: true, default: 'Insights' },
});

Insight.add('SEO', {
	snippet: { type: String, required: true, default: 'Insert 160 character meta-description here' },
});

Insight.register();
