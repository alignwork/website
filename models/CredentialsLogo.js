const keystone = require('keystone');
const Types = keystone.Field.Types;

/**
 * CredentialsLogo Model
 * ======================
 */

const CredentialsLogo = new keystone.List('CredentialsLogo', {
	map: { name: 'title' },
	autokey: { path: 'logoName', from: 'title', unique: true },
	track: true,
});

CredentialsLogo.add({
	title: { type: String, required: true, default: 'Logo' },
	imageUrl: { type: Types.Url },
	upload: { type: Types.CloudinaryImage },
});

CredentialsLogo.register();
