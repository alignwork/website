const keystone = require('keystone');

/**
 * Ebook Model
 * ===========
 */

const Ebook = new keystone.List('Ebook', {
	map: { name: 'title' },
	autokey: { path: 'ebook', from: 'title', unique: true },
	track: true,
	label: 'Ebooks',
});

Ebook.add({
	title: { type: String, required: true, default: 'Ebook-' },
	heading: { type: String, required: true, default: 'Download the free Align.Work e-book “OKR - a new performance management approach”' },
	button: {
		body: { type: String, required: true, default: 'Download' },
	},
	modal: {
		heading: { type: String, required: true, default: 'Download this FREE eBook' },
		button: {
			body: { type: String, required: true, default: 'Download eBook' },
		},
	},
});

Ebook.register();
