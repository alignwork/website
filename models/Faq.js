const keystone = require('keystone');
const Types = keystone.Field.Types;

/**
 * Faq Model
 * =========
 */

const Faq = new keystone.List('Faq', {
	map: { name: 'title' },
	autokey: { path: 'faq', from: 'title', unique: true },
	track: true,
	label: 'FAQ',
});

Faq.add('Content', {
	title: { type: String, required: true, default: 'Align.Work | FAQ' },
	heading: { type: String, required: true, default: 'FAQ' },
	subHeading: { type: String, required: true, default: 'Some intro copy here' },
	faqItems: { type: Types.Relationship, ref: 'FaqItem', many: true },
});

Faq.add('SEO', {
	snippet: { type: String, required: true, default: 'Insert 160 character meta-description here' },
});

Faq.register();
