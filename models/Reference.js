const keystone = require('keystone');
const Types = keystone.Field.Types;

/**
 * Reference Model
 * ===============
 */

const Reference = new keystone.List('Reference', {
	map: { name: 'title' },
	autokey: { path: 'reference', from: 'title', unique: true },
	track: true,
	label: 'References',
});

Reference.add({
	title: { type: String, required: true, default: '-reference' },
	imageUrl: { type: Types.Url },
	upload: { type: Types.CloudinaryImage },
	quote: { type: String, required: true, default: 'Quotation here...' },
	citation: { type: String, required: true, default: 'Quote owner here...' },
	link: { type: Types.Url },
});

Reference.register();
