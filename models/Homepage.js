const keystone = require('keystone');
const Types = keystone.Field.Types;

/**
 * Homepage Model
 * ==============
 */

const Homepage = new keystone.List('Homepage', {
	map: { name: 'title' },
	autokey: { path: 'pageName', from: 'title', unique: true },
	track: true,
	label: 'Homepage',
});

Homepage.add({
	title: { type: String, required: true, default: 'Homepage' },
});

Homepage.add('CarouselSlides', {
	slides: { type: Types.Relationship, ref: 'CarouselSlide', many: true },
});

Homepage.add('Splash', {
	splash: {
		heading: { type: String, required: false, default: 'Splash heading' },
		subHeading: { type: String, required: false, default: 'Splash subheading' },
		body: { type: Types.Html, wysiwyg: true, required: false, default: 'Test to support the image' },
		upload: { type: Types.CloudinaryImage },
		imageUrl: { type: Types.Url },
		button: {
			body: { type: String, required: true, default: 'Click me' },
			link: { type: Types.Url },
		},
	},
});

Homepage.add('Introduction', {
	intro: {
		heading: { type: String, required: true, default: 'Clear goals, engaged staff, faster results' },
		subHeading: { type: String, required: true, default: 'Objectives and Key Results (OKRs) is a management process sweeping Silicon Valley’s best companies.' },
		body: { type: Types.Html, wysiwyg: true, required: true, default: 'Nullam quis risus eget urna mollis ornare vel eu leo. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nullam id dolor id nibh ultricies vehicula. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.' },
		upload: { type: Types.CloudinaryImage },
		imageUrl: { type: Types.Url },
/*
			panel: {
			heading: { type: String, required: true, default: 'OKR coaching with Marshall King' },
			upload: { type: Types.CloudinaryImage },
			imageUrl: { type: Types.Url },
			body: { type: String, required: true, default: 'Nullam quis risus eget urna mollis ornare vel eu leo. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nullam id dolor id nibh ultricies vehicula. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.' },
			button: {
				body: { type: String, required: true, default: 'Learn more' },
				link: { type: Types.Url },
			},
		}, removed for Marshall */

		button: {
			body: { type: String, required: true, default: 'Learn more' },
			link: { type: Types.Url },
		},
	},
});

Homepage.add('Benefits', {
	benefits: {
		heading: { type: String, required: true, default: 'Key benefits' },
		subHeading: { type: String, required: true, default: 'How we can help' },
		body: { type: Types.Html, wysiwyg: true, required: true, default: 'Do you recognise these problems .... here is how we can help ...' },
		upload: { type: Types.CloudinaryImage },
		imageUrl: { type: Types.Url },
		button: {
			body: { type: String, required: true, default: 'Learn more' },
			link: { type: Types.Url },
		},
	},
});

/*
Homepage.add('Creds', {
	creds: {
		heading: { type: String, required: true, default: 'Top managers and companies use OKRs' },
		subHeading: { type: String, required: true, default: 'Thousands of great companies use the OKR goal setting methodology' },
		logos: { type: Types.Relationship, ref: 'CredentialsLogo', many: true },
		testimonials: { type: Types.Relationship, ref: 'Testimonial', many: true },
	},
});
*/
Homepage.add('References', {
	reference: { type: Types.Relationship, ref: 'Reference', many: true },
});

Homepage.add('Coaching', {
	coaching: {
		heading: { type: String, required: true, default: 'Why use a goal setting coach?' },
		subHeading: { type: String, required: true, default: 'A coach for the first few cycles will ensure the process is done right and embedded into the organisation culture.' },
		list: { type: Types.Html, wysiwyg: true, default: 'Hello world' },
		button: {
			body: { type: String, required: true, default: 'Learn more' },
			link: { type: Types.Url },
		},
/*
	*	upload: { type: Types.CloudinaryImage },
	*	imageUrl: { type: Types.Url }, removed for Marshall  */

		panel: {
			heading: {type: String, required: true, default: 'OKR coaching with Marshall King'},
			upload: {type: Types.CloudinaryImage},
			imageUrl: {type: Types.Url},
			body: {
				type: String,
				required: true,
				default: 'Nullam quis risus eget urna mollis ornare vel eu leo. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nullam id dolor id nibh ultricies vehicula. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.'
			},
			button: {
				body: {type: String, required: true, default: 'Learn more'},
				link: {type: Types.Url},
			},
		},
	},
});

Homepage.add('SEO', {
	snippet: { type: String, required: true, default: 'Insert 160 character meta-description here' },
});

Homepage.defaultColumns = 'title, createdAt, createdBy, updatedAt, updatedBy';

Homepage.register();
