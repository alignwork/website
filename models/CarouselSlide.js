const keystone = require('keystone');
const Types = keystone.Field.Types;

/**
 * CarouselSlide Model
 * ===================
 */

const CarouselSlide = new keystone.List('CarouselSlide', {
	map: { name: 'title' },
	autokey: { path: 'slideName', from: 'title', unique: true },
	track: true,
	label: 'Carousel Slides',
});

CarouselSlide.add({
	title: { type: String, required: true, default: 'Slide' },
	imageUrl: { type: Types.Url },
	upload: { type: Types.CloudinaryImage },
	heading: { type: String, required: true, default: 'Heading' },
	subHeading: { type: String, required: true, default: 'Sub Heading' },
	button: {
		body: { type: String, required: true, default: 'Learn more' },
		link: { type: Types.Url },
	},
});

CarouselSlide.register();
