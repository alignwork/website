'use strict';

const MD = require('./mandrill');

class mailer {

	static async sendSubscribeEmail (intCat, email, firstName) {
		switch (intCat) {
			case 'Insights':
				return await MD.sendConfirmation(email, firstName, 'confirm-insights');
			case 'Coaching':
				return await MD.sendConfirmation(email, firstName, 'confirm-coaching');
			case 'eBook':
				return await MD.sendConfirmation(email, firstName, 'confirm-ebook');
		}
	}

	static async sendDoubleOptIn (intCat, email, firstName) {
		return await MD.sendDoubleOptIn(email, firstName, intCat);
	}

	static async sendTrialNotification (email, userName, teamName) {
		return await MD.sendTrialNotfication(email, userName, teamName);
	}

}

module.exports = mailer;
