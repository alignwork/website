'use strict';

const request = require('superagent');

class mandrill {

	static async sendConfirmation (email, firstName, templateName) {

		let result;
		try {
			result = await request
				.post(process.env.MANDRILL_API_ENDPOINT + '/messages/send-template.json')
				.send({
					key: process.env.MANDRILL_API_KEY,
					template_name: templateName,
					template_content: [],
					message: {
						to: [
							{
								email: email,
							},
						],
						important: false,
						track_opens: true,
						track_clicks: true,
						auto_text: true,
						auto_html: true,
						inline_css: false,
						url_strip_qs: false,
						preserve_recipients: false,
						view_content_link: true,
						tracking_domain: null,
						signing_domain: null,
						return_path_domain: null,
						merge: true,
						merge_language: 'mailchimp',
						global_merge_vars: [
							{
								name: 'FNAME',
								content: firstName,
							},
						],
						tags: [],
					},
					async: false,
				});
		}
		catch (e) {
			throw e;
		}

		return result;
	}

	static async sendDoubleOptIn (email, firstName, intCat) {

		let result;
		try {
			result = await request
				.post(process.env.MANDRILL_API_ENDPOINT + '/messages/send-template.json')
				.send({
					key: process.env.MANDRILL_API_KEY,
					template_name: 'confirm-subscribe',
					template_content: [],
					message: {
						to: [
							{
								email: email,
							},
						],
						important: false,
						track_opens: true,
						track_clicks: true,
						auto_text: true,
						auto_html: true,
						inline_css: false,
						url_strip_qs: false,
						preserve_recipients: false,
						view_content_link: true,
						tracking_domain: null,
						signing_domain: null,
						return_path_domain: null,
						merge: true,
						merge_language: 'mailchimp',
						global_merge_vars: [
							{
								name: 'EMAIL',
								content: email,
							}, {
								name: 'INTCAT',
								content: intCat,
							},
							{
								name: 'FNAME',
								content: firstName,
							},
						],
						tags: [],
					},
					async: false,
				});
		}
		catch (e) {
			throw e;
		}

		return result;
	}

	static async sendTrialNotfication (email, userName, teamName) {

		let result;
		try {
			result = await request
				.post(process.env.MANDRILL_API_ENDPOINT + '/messages/send-template.json')
				.send({
					key: process.env.MANDRILL_API_KEY,
					template_name: 'free-trial-notification',
					template_content: [],
					message: {
						to: [
							{
								email: 'marshall.king@align.work',
							}, {
								email: 'jon.wade@align.work',
							}, {
								email: 'dave.muskett@align.work',
							},
						],
						important: false,
						track_opens: true,
						track_clicks: true,
						auto_text: true,
						auto_html: true,
						inline_css: false,
						url_strip_qs: false,
						preserve_recipients: false,
						view_content_link: true,
						tracking_domain: null,
						signing_domain: null,
						return_path_domain: null,
						merge: true,
						merge_language: 'mailchimp',
						global_merge_vars: [
							{
								name: 'EMAIL',
								content: email,
							}, {
								name: 'UNAME',
								content: userName,
							}, {
								name: 'TNAME',
								content: teamName,
							},
						],
						tags: [],
					},
					async: false,
				});
		}
		catch (e) {
			// console.log(e);
			throw e;
		}

		// console.log(result);
		return result;
	}

}

module.exports = mandrill;
