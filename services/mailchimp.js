'use strict';

const request = require('superagent');
const pluck = require('lodash/fp/pluck');
const concat = require('lodash/fp/concat');

class mailChimp {

	static async addMember (email, intCat, firstName, lastName, fullName, userName, teamName) {

		let interests = {};
		if (intCat === process.env.MAILCHIMP_AWML_TRIAL_ID) {
			interests = {
				[intCat]: true,
				[process.env.MAILCHIMP_AWML_INSIGHTS_ID]: true,
			};
		}
		else {
			interests = {
				[intCat]: true,
			};
		}

		let result;
		try {
			result = await request
				.post(process.env.MAILCHIMP_API_ENDPOINT + '/lists/' + process.env.MAILCHIMP_AWML_LIST_ID + '/members')
				.send({
					email_address: email,
					status: 'subscribed',
					merge_fields: {
						FNAME: firstName ? firstName : '',
						LNAME: lastName ? lastName : '',
						UNAME: userName ? userName : '',
						TNAME: teamName ? teamName : '',
						FULLNAME: fullName ? fullName : '',
						DOPTIN: (intCat === process.env.MAILCHIMP_AWML_TRIAL_ID) ? 1 : 0,
						INTCAT: intCat,
					},
					interests: interests,
				});
		}
		catch (e) {
			throw e;
		}

		return result;
	}

	static async editMemberInterests (id, intCat, userName, teamName) {

		let mergeFieldsObj = {
			DOPTIN: 1,
		};

		if (userName) mergeFieldsObj.UNAME = userName;
		if (teamName) mergeFieldsObj.TNAME = teamName;

		let result;
		try {
			result = await request
				.patch(process.env.MAILCHIMP_API_ENDPOINT + '/lists/' + process.env.MAILCHIMP_AWML_LIST_ID + '/members/' + id)
				.send({
					merge_fields: mergeFieldsObj,
					interests: {
						[intCat]: true,
					},
				});
		}
		catch (e) {
			throw e;
		}

		return result;
	}

	static async subscribeMember (id) {

		let result;
		try {
			result = await request
				.patch(process.env.MAILCHIMP_API_ENDPOINT + '/lists/' + process.env.MAILCHIMP_AWML_LIST_ID + '/members/' + id)
				.send({
					merge_fields: {
						DOPTIN: 1,
					},
				});
		}
		catch (e) {
			throw e;
		}

		return result;
	}


	static async callMemberEndpoint (offset, count) {
		let result;
		try {
			result = await request
				.get(process.env.MAILCHIMP_API_ENDPOINT + '/lists/' + process.env.MAILCHIMP_AWML_LIST_ID + '/members')
				.query({
					count: count,
					offset: offset,
				});
		}
		catch (e) {
			throw e;
		}

		return result.body;
	}

	static async getMemberDetails () {

		let data = {
			ids: [],
			emails: [],
			interests: [],
		};
		let count = 100;
		let totalItems;

		try {
			let results = await mailChimp.callMemberEndpoint(0, count);
			data.ids = concat(data.ids, pluck('id', results.members));
			data.emails = concat(data.emails, pluck('email_address', results.members));
			data.interests = concat(data.interests, pluck('interests', results.members));
			totalItems = results.total_items;
		}
		catch (e) {
			throw e;
		}

		let totalRequestRequired = Math.ceil(totalItems / count);
		if (totalRequestRequired > 1) {
			for (let i = 1; i < totalRequestRequired; i++) {
				try {
					let results = await mailChimp.callMemberEndpoint(count * i, count);
					data.ids = concat(data.ids, pluck('id', results.members));
					data.emails = concat(data.emails, pluck('email_address', results.members));
					data.interests = concat(data.interests, pluck('interests', results.members));
				}
				catch (e) {
					throw e;
				}
			}
		}

		return data;
	}
}

module.exports = mailChimp;
