FROM node:8.0.0
LABEL maintainer = "jon.wade@align.work"
RUN mkdir -p /usr/src/website
WORKDIR /usr/src/website
COPY ./ ./
CMD ["node", "keystone"]
